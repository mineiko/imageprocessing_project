import os
import numpy as np
from numpy.lib.function_base import append
import torch
from PIL import Image
import xmltodict

class BloodDataset(object):
    def __init__(self, transforms, _type='train'):
        self.transforms = transforms
        if _type == 'train':
            self.base_path = "../CBC/Training/Images"
            self.base_path_xml = "../CBC/Training/Annotations"
        elif _type == 'test':
            self.base_path = "../CBC/Testing/Images"
            self.base_path_xml = "../CBC/Testing/Annotations"
        elif _type == 'val': 
            self.base_path = "../CBC/Validation/Images"
            self.base_path_xml = "../CBC/Validation/Annotations"
        self.imgs = [s for s in list(sorted(os.listdir(self.base_path))) if s[0] != '_']
        self.xmls = [s for s in list(sorted(os.listdir(self.base_path_xml))) if s[0] != '_']

    def getOriginalImage(self, idx):
        img_path = os.path.join(self.base_path, self.imgs[idx])
        img = Image.open(img_path).convert("RGB")
        return np.array(img)

    def __getitem__(self, idx):
        img_path = os.path.join(self.base_path, self.imgs[idx])
        img = Image.open(img_path).convert("RGB")

        with open(os.path.join(self.base_path_xml, self.xmls[idx])) as fd:
            doc = xmltodict.parse(fd.read())

        num_objs = 0
        boxes = []
        labels = []

        if str(doc['annotation']['object'].__class__) == '<class \'collections.OrderedDict\'>':
            num_objs = 1
            xmin = int(doc['annotation']['object']['bndbox']['xmin'])
            ymin = int(doc['annotation']['object']['bndbox']['ymin'])
            xmax = int(doc['annotation']['object']['bndbox']['xmax'])
            ymax = int(doc['annotation']['object']['bndbox']['ymax'])
            boxes.append([xmin, ymin, xmax, ymax])
            if doc['annotation']['object']['name'] == 'WBC':
                labels.append(0)
            elif doc['annotation']['object']['name'] == 'RBC':
                labels.append(1)
        else:
            num_objs = len(doc['annotation']['object'])
            for i in range(num_objs):
                if (doc['annotation']['object'][i]['name'] in ['WBC', 'RBC', 'Platelets']):
                    xmin = int(doc['annotation']['object'][i]['bndbox']['xmin'])
                    ymin = int(doc['annotation']['object'][i]['bndbox']['ymin'])
                    xmax = int(doc['annotation']['object'][i]['bndbox']['xmax'])
                    ymax = int(doc['annotation']['object'][i]['bndbox']['ymax'])
                    boxes.append([xmin, ymin, xmax, ymax])
                    if doc['annotation']['object'][i]['name'] == 'WBC':
                        labels.append(0)
                    elif doc['annotation']['object'][i]['name'] == 'RBC':
                        labels.append(1)
                    elif doc['annotation']['object'][i]['name'] == 'Platelets':
                        labels.append(2)
            

        boxes = torch.as_tensor(boxes, dtype=torch.float32)

        labels = torch.as_tensor(labels, dtype=torch.int64)

        image_id = torch.tensor([idx])

        target = {}
        target["boxes"] = boxes
        target["labels"] = labels
        target["image_id"] = image_id

        if self.transforms is not None:
            img, target = self.transforms(img, target)

        return img, target
    
    def __len__(self):
        return len(self.imgs)
        