# Image Processing Project

- ### Project Title: Segmenting and counting cells between red blood cells and white blood cells in microscope images

- ### Name: Gianella Arlette Milon Guzman

- ### Num USP: 12126062

- ### Course Code: SCC5830 (MSc/PhD student)

- ### Year/Semestre: 2021-I

## Abstract:

...


## The main objective of the project:

Identify and counting red (RBC) and white (WBC) blood cells in microscopic photographs using image processing tecniques; and, if possible, identify platelets too. 

## database:
A database named _complete blood count (CBC) dataset_ have 360 images of blood smear images. It was created and is maintained by Mahmudul Alam. The dataset is modified and prepared for the paper of _automatic identification and counting of blood cells_, this paper use maching learning and the framework "you only look once" (YOLO) to identify the cells of the image, like a next image: 

![alt text](https://user-images.githubusercontent.com/37298971/46539603-c77ab900-c8d8-11e8-9e48-e6c054f8af3b.jpg)

But the original dataset have 410 images.

- Website: [https://mahmudulalam.github.io/Complete-Blood-Cell-Count-Dataset/](url)
- Paper: [https://ietresearch.onlinelibrary.wiley.com/doi/10.1049/htl.2018.5098](url)
- GitHub: [https://github.com/MahmudulAlam/Complete-Blood-Cell-Count-Dataset](url)

## Image Processing Steps
1. Edge-detection: To find the shape of the cell.
2. Image Segmentation: Separate a cell of all of them, and if possible use matematical morphology to make easier the next step.
3. Color Analysis: Experiments like, extract a color component to gray scales to identify the WBC (white cells), use histograms to differentiate between cells.
4. Identify a different cell in the image based on the color Analysis

## Application: 

Computational photography

## Initial code with first results:

In the next link have a little process: [First_Steps.ipynb](First_Steps.ipynb)


